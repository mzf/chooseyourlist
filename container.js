angular.module('ChooseYourListApp', [
'ChooseYourListApp.controllers',
'ChooseYourListApp.services'
]);

angular.module('ChooseYourListApp.controllers', []).
controller('questionController', function($scope, $window, questionAPIService) {

	//--- initialization ---
	$scope.question = {id:'start', text:'Loading...'};
	questionAPIService.getNextQuestion('start', true).success(function (response) {
		$scope.question = response;
	});
	$scope.breadcrumbs = [];

	//--- update to the next question ---
	$scope.updateToNextQuestion = function(isYes) {
		questionAPIService.getNextQuestion($scope.question.id, isYes).success(function (response) {
			$scope.breadcrumbs.push($scope.question);
			$scope.question = response;
		});
	};

	//--- restart the test ---
	$scope.restart = function() {
		$window.location.reload();
	};

	//--- show the previous question ---
	$scope.previous = function() {
		if($scope.breadcrumbs.length > 0) {
			$scope.question = $scope.breadcrumbs.pop();
		}
	};
});

angular.module('ChooseYourListApp.services', []).
factory('questionAPIService', function($http) {

	var questionAPI = {};

	questionAPI.getNextQuestion = function(currentId, val) {
		return $http({
			method: 'GET',
			url: 'data.php?c='+currentId+'&r='+val
		});
	}

	return questionAPI;
});
