<?php

$currentState = (isset($_GET['c'])) ? $_GET['c'] : NULL;
$answer = (isset($_GET['r'])) ? $_GET['r'] : NULL;

//the logic steps
$nextStep = array(	'order_is_important' => array(	'true' =>	'last_in_first_out',
													'false' =>	'need_to_find_element_by_key'),

					'last_in_first_out' => array(	'true' =>	'stack',
													'false' =>	'first_in_first_out'),

					'first_in_first_out' => array(	'true' =>	'queue',
													'false' =>	'largest_element_first_out'),

					'largest_element_first_out' => array(	'true' => 	'priority_queue',
															'false' =>	'sorted_by_key'),

					'sorted_by_key' => array(	'true' =>	'allow_duplicates',
												'false' =>	'insert_erase_in_middle'),

					'allow_duplicates' => array(	'true' =>	'store_key_separate_to_value_multi',
													'false' =>	'store_key_separate_to_value'),

					'store_key_separate_to_value' => array(	'true' =>	'map',
															'false' =>	'set'),

					'store_key_separate_to_value_multi' => array(	'true' =>	'multi_map',
																	'false' =>	'multi_set'),

					'insert_erase_in_middle' => array(	'true' =>	'list',
														'false' =>	'insert_erase_at_front'),

					'insert_erase_at_front' => array(	'true' =>	'need_to_merge_collections1',
														'false' =>	'need_to_find_the_nth_element'),

					'need_to_merge_collections1' =>	array(	'true' =>	'list',
															'false' =>	'deque'),

					'need_to_find_the_nth_element' => array(	'true' =>	'size_will_vary_widely',
																'false' =>	'need_to_merge_collections2'),

					'size_will_vary_widely' => array(	'true' =>	'deque',
														'false' =>	'vector'),

					'need_to_merge_collections2' => array(	'true' =>	'list',
															'false' =>	'size_will_vary_widely'),

					'need_to_find_element_by_key' => array(	'true' =>	'allow_duplicates',
															'false' =>	'need_to_merge_collections2')
					);

//text for the questions
$questions = array(	'order_is_important' => 'Order is important?',
					'last_in_first_out' => 'Last In First Out?',
					'first_in_first_out' => 'First In First Out?',
					'largest_element_first_out' => 'Largest element first out?',
					'sorted_by_key' => 'Sorted by key?',
					'allow_duplicates' => 'Allow duplicates?',
					'store_key_separate_to_value' => 'Store key separate to value?',
					'store_key_separate_to_value_multi' => 'Store key separate to value?',
					'insert_erase_in_middle' => 'Insert or erase in the middle?',
					'insert_erase_at_front' => 'Insert or erase at front?',
					'need_to_merge_collections1' =>	'Need to merge collections?',
					'need_to_find_the_nth_element' => 'Need to find the Nth element?',
					'size_will_vary_widely' => 'Size will vary widely?',
					'need_to_merge_collections2' => 'Need to merge collections?',
					'need_to_find_element_by_key' => 'Need to find element by key?'
					);

//image for the questions
$images = array(	'order_is_important' => 'order_is_important',
					'last_in_first_out' => 'last_in_first_out',
					'first_in_first_out' => 'first_in_first_out',
					'largest_element_first_out' => 'largest_element_first_out',
					'sorted_by_key' => 'sorted_by_key',
					'allow_duplicates' => 'allow_duplicates',
					'store_key_separate_to_value' => 'store_key_separate_to_value',
					'store_key_separate_to_value_multi' => 'store_key_separate_to_value',
					'insert_erase_in_middle' => 'insert_erase_in_middle',
					'insert_erase_at_front' => 'insert_erase_at_front',
					'need_to_merge_collections1' =>	'need_to_merge_collections',
					'need_to_find_the_nth_element' => 'need_to_find_the_nth_element',
					'size_will_vary_widely' => 'size_will_vary_widely',
					'need_to_merge_collections2' => 'need_to_merge_collections',
					'need_to_find_element_by_key' => 'need_to_find_element_by_key'
					);

//the final containers
$containers = array(	'stack' => array(	"link" =>	"http://www.cplusplus.com/reference/stack/stack/",
											"image" =>	"stack"),
											
						'queue' => array(	"link" =>	"http://www.cplusplus.com/reference/queue/queue/",
											"image" =>	"queue"),
											
						'priority_queue' => array(	"link" =>	"http://www.cplusplus.com/reference/queue/priority_queue/",
													"image" =>	"priority_queue"),
													
						'map' => array(	"link" =>	"http://www.cplusplus.com/reference/map/map/",
													"image" =>	"map"),
													
						'set' => array(	"link" =>	"http://www.cplusplus.com/reference/set/set/",
													"image" =>	"set"),
													
						'multi_map' => array(	"link" =>	"http://www.cplusplus.com/reference/map/multimap/",
												"image" =>	"multi_map"),
												
						'multi_set' => array(	"link" =>	"http://www.cplusplus.com/reference/set/multiset/",
												"image" =>	"multi_set"),
												
						'list' => array(	"link" =>	"http://www.cplusplus.com/reference/list/list/",
											"image" =>	"list"),
											
						'vector' => array(	"link" =>	"http://www.cplusplus.com/reference/vector/vector/",
											"image" =>	"vector"),
											
						'deque' => array(	"link" =>	"http://www.cplusplus.com/reference/deque/deque/",
											"image" =>	"deque")
						);

if (($currentState != null) && ($answer != null))
{
	if($currentState == 'start') //first question
	{
		$nextId = 'order_is_important';
	}
	else
	{
		//get the next step
		$nextId = $nextStep[$currentState][$answer];
	}
	
	//check for last step
	if(array_key_exists($nextId, $containers)) //last step
	{
		$final_container = $containers[$nextId];
		$link = $final_container["link"];
		$image = $final_container["image"];
		print '{';
		print '"isFinal":"true",';
		print '"container":"' . $nextId .'",';
		print '"link":"'. $link .'",';
		print '"img":"'. $image .'"';
		print '}';
	}
	else	
	{
		$nextQuestion = $questions[$nextId];
		$nextImage = $images[$nextId];
		print '{';
		print '"id":"' . $nextId .'",';
		print '"text":"'. $nextQuestion .'",';
		print '"img":"'. $nextImage .'"';
		print '}';
	}
}
else
{
	print 'oops!';
}

?>
